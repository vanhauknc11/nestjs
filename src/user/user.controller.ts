import { CreateUserDto, createUserSchema } from './dto/createUserSchema';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UsePipes,
} from '@nestjs/common';
import { UserService } from './user.service';
import { SuccessResponse } from 'src/responses.type/success.response';

import { ZodValidationPipe } from 'src/utils/ZodValidationPipe';
import { UpdateUserDto } from './dto/updateUserSchema';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get(':id')
  async getUser(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<SuccessResponse> {
    return new SuccessResponse({
      metadata: await this.userService.user({ id }),
    }).send();
  }

  @Get()
  async getUsers(): Promise<SuccessResponse> {
    return new SuccessResponse({
      metadata: await this.userService.users(),
    }).send();
  }

  @Post()
  @UsePipes(new ZodValidationPipe(createUserSchema))
  async createUser(@Body() userDto: CreateUserDto): Promise<SuccessResponse> {
    return new SuccessResponse({
      metadata: await this.userService.createUser(userDto),
    }).send();
  }

  @Put(':id')
  // @UsePipes(new ZodValidationPipe(updateUserSchema))
  async updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<SuccessResponse> {
    return new SuccessResponse({
      metadata: await this.userService.updateUser({
        where: { id },
        data: updateUserDto,
      }),
    }).send();
  }

  @Delete(':id')
  async deleteUser(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<SuccessResponse> {
    return new SuccessResponse({
      metadata: await this.userService.deleteUser({ id }),
    }).send();
  }
}
