import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Prisma, User } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class UserService {
  constructor(private readonly prismaService: PrismaService) {}
  async users(): Promise<User[]> {
    return this.prismaService.user.findMany({});
  }
  async user(id: Prisma.UserWhereUniqueInput): Promise<User | null> {
    return this.prismaService.user.findUnique({
      where: id,
    });
  }
  async createUser(data: Prisma.UserCreateInput): Promise<User> {
    const isEmailExist = await this.prismaService.user.findUnique({
      where: { email: data.email },
    });
    if (isEmailExist)
      throw new HttpException('Email already exist', HttpStatus.CONFLICT);
    return this.prismaService.user.create({ data });
  }
  async updateUser(params: {
    where: Prisma.UserWhereUniqueInput;
    data: Prisma.UserUpdateInput;
  }) {
    const { where, data } = params;
    return this.prismaService.user.update({ data, where });
  }
  async deleteUser(where: Prisma.UserWhereUniqueInput): Promise<User> {
    const user = await this.prismaService.user.findFirst({ where });
    if (!user) throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    return this.prismaService.user.delete({ where });
  }
}
