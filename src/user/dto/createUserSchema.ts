import { z } from 'zod';

export const createUserSchema = z
  .object({
    email: z.string(),
    name: z.string(),
  })
  .required();

export type CreateUserDto = z.infer<typeof createUserSchema>;
