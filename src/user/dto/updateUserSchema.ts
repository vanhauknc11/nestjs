import { z } from 'zod';

export const updateUserSchema = z
  .object({
    email: z.string(),
    name: z.string(),
  })
  .required();

export type UpdateUserDto = z.infer<typeof updateUserSchema>;
