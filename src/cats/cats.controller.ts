import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UsePipes,
} from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';
import { ZodValidationPipe } from 'src/utils/ZodValidationPipe';
import { createCatSchema } from './dto/createCatSchema';
import { SuccessResponse } from 'src/responses.type/success.response';

@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Post()
  @UsePipes(new ZodValidationPipe(createCatSchema))
  async create(@Body() createCatDto: CreateCatDto): Promise<SuccessResponse> {
    this.catsService.create(createCatDto);
    return new SuccessResponse({
      message: 'Created Cat!!',
      metadata: createCatDto,
    }).send();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number): SuccessResponse {
    return new SuccessResponse({
      message: `This is cat ID#${id}`,
    }).send();
  }

  @Get()
  findAll(): SuccessResponse {
    const casts = this.catsService.findAll();
    return new SuccessResponse({
      message: 'That all my Cats',
      metadata: casts,
    }).send();
  }
}
