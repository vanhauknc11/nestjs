interface SuccessResponseType {
  statusCode?: number;
  message?: string;
  metadata?: any;
}

export class SuccessResponse {
  private statusCode: number;
  private message: string;
  private metadata: any;
  constructor({
    statusCode = 200,
    message = 'Successfully!',
    metadata = {},
  }: SuccessResponseType) {
    this.statusCode = statusCode;
    this.message = message;
    this.metadata = metadata;
  }
  public send() {
    return this;
  }
}
